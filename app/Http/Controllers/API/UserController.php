<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::latest()->paginate(10);
        return $users;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:191',
            'email' => 'required|unique:users|email|string|max:191',
            'password' => 'required|string|min:8',
            'type' => 'required|string',
        ]);
        // return $request->all();
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'type' => $request['type'],
            'bio' => $request['bio'],
            'photo' => $request['photo'],
            'password' => Hash::make($request['password']),
        ]);

        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:191',
            'email' => 'required|email|string|max:191|unique:users,email,'.$id,
            'password' => 'sometimes|string|min:8',
            'type' => 'required|string',
        ]);
        
        if(!empty($request->password)){
            $request->merge(['password' => Hash::make($request->password)]);
        }

        $user = User::findOrFail($id);
        $user->update($request->all());
        return ["message"=>"User has been updated"];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return ['message'=>'User has been deleted'];
    }

    public function profile()
    {
        return auth('api')->user();
    }

    public function updateProfile(Request $request)
    {
        $user = auth('api')->user();

        $validatedData = $request->validate([
            'name' => 'required|string|max:191',
            'email' => 'required|email|string|max:191|unique:users,email,'.$user->id,
            'password' => 'sometimes|string|min:8',
            'bio' => 'sometimes|string|max:255',
        ]);
        
        if($request->photo != $user->photo){
            $imgName = $this->base64ToImage($request->photo);
            $request->merge(['photo'=>$imgName]);

            $crntPhoto = public_path('img/profile/').$user->photo;
            if(file_exists($crntPhoto)){
                unlink($crntPhoto);
            }

        }

        if(!empty($request->password)){
            $request->merge(['password' => Hash::make($request->password)]);
        }
       
        $user->update($request->all());
        return ['message'=>'Success'];
        
    }


    public function base64ToImage($image) {
        $name = time() . "." . explode("/", explode(";",$image)[0])[1];
            \Image::make($image)->save(public_path('img/profile/').$name);

        return $name;
    }
}
