<?php


namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return $products;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'code' => 'required|unique:products|string|max:191',
            'name' => 'required|unique:products|string|max:191',
            'description' => 'required|string|max:255',
            'cost' => 'required|integer',
            'price' => 'required|integer',
        ]);
        Product::create([
            'code'=>$request['code'],
            'name'=>$request['name'],
            'description'=>$request['description'],
            'cost'=>$request['cost'],
            'price'=>$request['price'],
        ]);

        return ['message'=>'Product has been successfully added.'];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $validatedData = $request->validate([
            'code' => 'required|unique:products,code,'.$product->id.'|string|max:191',
            'name' => 'required|unique:products,name,'.$product->id.'|string|max:191',
            'description' => 'required|string|max:255',
            'cost' => 'required|integer',
            'price' => 'required|integer',
        ]);
        $product->update($request->all());
        return ['message'=>'Product has been successfully update.'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return ['message'=>'User has been deleted'];
    }
}
