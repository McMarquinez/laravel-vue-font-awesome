@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1><strong>Coronavirus Disease 2019 Updates</strong></h1>
            <hr>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header border-0">
                  <h3 class="card-title">Countries</h3>
                </div>
                <div class="card-body p-0">
                  <table class="table table-striped table-valign-middle">
                    <thead>
                        <tr>
                        <th>Countries</th>
                        <th>Total Cases</th>
                        <th>Today's Cases</th>
                        <th>More</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <td>
                            <img src="https://raw.githubusercontent.com/NovelCOVID/API/master/assets/flags/ph.png" alt="country" class="img-size-32 mr-2">
                            Philippines
                        </td>
                        <td>1418</td>
                        <td>
                            <small class="text-success mr-1">
                            <i class="fas fa-arrow-up"></i>
                            17%
                            </small>
                            343
                        </td>
                        <td>
                            <a href="#" class="text-muted">
                            <i class="fas fa-search"></i>
                            </a>
                        </td>
                        </tr>
                    </tbody>
                  </table>
                </div>
              </div>
        </div>
    </div>
</div>
@endsection
