<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'ref_no',
        'status',
        'supplier',
    ];

    public function items(){
        return $this->hasMany('App\OrderItem','order_item_id','id');
    }
}
